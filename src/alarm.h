//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

//
// Alarms handling
//

#ifndef _ALARM_H_
#define _ALARM_H_ 1

#include "list.h"

enum AlarmOperation {
    ALARM_CLEAN = 0,
    ALARM_CHECK
};

void        manageAlarms (int mode);
SingleList* getAlarms    (void);
void        forgetAlarms (void);    // just set to NULL, no cleanup
void        addAlarm     (char *file, char *macro);
void        freeAlarms   (SingleList* chain);

void keepaliveTest(void);
void addKeepalive(const char* tmout);
void setKeepalive();
boolean_t useKeepalive();

#endif
