//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#include "common.h"
#include "utils.h"
#include "pr_stdin.h"

extern char tmp[MAXMAXLEN];

//
// Support for stdin non-blocking IO (actually only Input)
//
// need a non-blocking IO because of another connection to front-end
// at the same time
//
#define NB_ENABLE  1
#define NB_DISABLE 0

static char stdinbuf[1024];
static int  cnt = 0;

static int kbhit()
{
    struct timeval tv;
    fd_set fds;
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds); //STDIN_FILENO is 0
    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
    return FD_ISSET(STDIN_FILENO, &fds);
}

static void nonblock(int state)
{
    struct termios ttystate;

    //get the terminal state
    tcgetattr(STDIN_FILENO, &ttystate);

    if (state==NB_ENABLE) {
        //turn off canonical mode
        ttystate.c_lflag &= ~ICANON;
        //minimum of number input read.
        ttystate.c_cc[VMIN] = 1;
    } else if (state==NB_DISABLE) {
        //turn on canonical mode
        ttystate.c_lflag |= ICANON;
    }
    //set the terminal attributes.
    tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);

}

int stdinOpen(ConnectInfo* conn)
{
    //printf("stdinOpen\n");
    DEBUG2("[DS]: Server mode. Use standard input");
    nonblock(NB_ENABLE);
    
    conn->state = PEER_CONNECTED;
    return EXIT_OK;
}

void stdinClose(ConnectInfo* conn, int final)
{
    if (final) {
	    //printf("stdinClose\n");
	    nonblock(NB_DISABLE);
    }
    conn->state = PEER_DISCONNECTED;
}

void stdinReset(ConnectInfo* conn)
{
   stdinClose(conn, 0);
}

int stdinRead(char* buf, int max)
{
    int ch = '1';

    //nonblock(NB_ENABLE);

    while (kbhit() && cnt < max) {
        //printf("readStdin kbhit() OK\n");
        ch = getchar();
        //printf("readStdin got %d %c\n",cnt,ch);
        if (ch == '\n' || ch == EOF) {
            break;
        }

        stdinbuf[cnt] = ch;
        cnt++;
    }

    if (cnt == max || ch == '\n') {
        stdinbuf[cnt] = '\0';
        strcpy(buf,stdinbuf);
        int i = cnt;
        cnt = 0;
        return i;
    }
    
    buf[0] = '\0';
    return (ch == EOF ? EOF : 0);
}
