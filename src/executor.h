//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

#ifndef _EXECUTOR_H_
#define _EXECUTOR_H_

#include "lib_wrapper.h"

enum ExecMsgType {
    EM_KEY = 0,
    EM_STRING,
    EM_EVENT,
    EM_ALARM,
    EM_AS_IS
};

typedef struct em {
    int   peer;
    int   type;
    void* value;
} eMessage;

typedef struct {
    int   id;
    int   xSz;
    int   ySz;
    int   coverSz;
} PeerDef;

pointer_t executorRoutine	(pointer_t thread);
void  	 sendToExecutor  	(eMessage *buf);
void  	 freeEMessage  	    (void *buf);
void  	 sendEventToExecutor(int peer, int event);
void  	 sendToMainMenu		(int peer);

#endif
