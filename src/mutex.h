//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

//
// Mutex related wrappers
//

#ifndef _MUTEX_H_
#define _MUTEX_H_ 1

enum MutexID {
    M_LOG = 0,
    M_WEB,
    M_QUEUE,
    M_STATE,
    M_MAX
};

int   mutexNew   (int id);
void  mutexLock  (int id); 
void  mutexUnlock(int id); 
void  mutexRemove(int id);
int   mutexExists(int id);

#endif
