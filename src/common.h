//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

#ifndef _COMMON_H_
#define _COMMON_H_

#define EXIT_EXACT  -3
#define EXIT_NOK     0
#define EXIT_OK      1
#define EXIT_DISCON  2
#define EXIT_ABORT   3
#define EXIT_STOP    4
#define EXIT_INITOK  5

#define MAXMAXLEN    	2048
#define MAXLEN          512
#define MAXCKPDLEN    	100
#define MTEXTLEN    	30

#define IS_OK(answer) (strstr(answer, "OK") != NULL)

//#define DEFAULT_DEVICE  "/dev/rfcomm0"	seems JSR-82 phones is more usable now
//#define DEFAULT_DEVICE  "bluetooth"

#ifdef USE_AVAHI
  #if defined USE_BLUEZ || defined USE_BT_FBSD 
    #define DEFAULT_DEVICE  "bluetooth,tcp:5197,web:5080,avahi"
  #else
    #define DEFAULT_DEVICE  "tcp:5197,web:5080,avahi"
  #endif
#else
  #if defined USE_BLUEZ || defined USE_BT_FBSD 
    #define DEFAULT_DEVICE  "bluetooth:19,tcp:5197,web:5080"
  #else
    #define DEFAULT_DEVICE  "tcp:5197,web:5080"
  #endif
#endif

#define RFCOMM_DEVICE   "rfcomm"
#define AT_DEVICE   	"at:"
#define INET_SOCKET   	"socket:"
#define PEER_TCP   	    "tcp:"
#define BT_SOCKET   	"bluetooth"
#define BT_SPP   	    "btspp" 	
#define L2CAP_SOCKET   	"l2cap"  	// not yet used
#define UNIX_SOCKET   	"local:"
#define ILIRC_SOCKET   	"ilirc:"
#define STDIN_STREAM   	"stdin"
#define WEB_SOCKET   	"web:"
#define CMXML_SOCKET   	"cmxml:"
#define AVAHI_USE   	"avahi"

#define PROTO_AT        1
#define PROTO_ANYREMOTE 2
#define PROTO_BEMUSED   3
#define PROTO_IVIEWER   4

#define PEER_ANY        0

enum ProtocolMode {
    CLIENT_RFCOMM = 0,	 // AT protocol over bluetooth
    CLIENT_AT,	         // AT protocol over IR or cable
    SERVER_TCP,	         // anyRemote protocol over TCP/IP
    SERVER_BT,	         // anyRemote protocol over btspp
    CLIENT_ILIRC,	     // inputLirc protocol
    CLIENT_NOAT,	     // anyRemote protocol over IR (or any AF_UNIX socket) - as client
    SERVER_STDIN,	     // anyRemote protocol, read command from stdin
    SERVER_WEB,	         // anyRemote protocol, built-in web server
    SERVER_CMXML,	     // anyRemote protocol, built-in XML server
    #ifdef USE_L2CAP
    SERVER_L2CAP,	     // TODO: anyRemote protocol over l2cap
    #endif
    SERVER_UX ,  	     // NOT USED: anyRemote protocol over IR (or any AF_UNIX socket) - as server
    FRONT_END ,  	     // Used to communicate with frontend
    SERVER_MAX   	 
};

#define DEFAULT_BT_CHANNEL 1
#define BT_ADDR_LEN        17   // 00:12:EF:32:21:1A
#define DEFAULT_L2CAP_PORT 5019

#ifndef SOCK_CLOEXEC
#define SOCK_CLOEXEC 0
#endif

#define VAR_AUTOCONN   	  "AutoConnect"
#define VAR_AUTOREPEAT	  "AutoRepeat"
#define VAR_CFGDIR    	  "CfgDir"
#define VAR_CHARSET    	  "CharSet"
#define VAR_TO_ENCODING   "ToEncoding"
#define VAR_FROM_ENCODING "FromEncoding"
#define VAR_LOGGING       "Logging"
#define VAR_IVIEWER       "IViewer"
#define VAR_BEMUSED       "Bemused"
#define VAR_WAITEXEC      "WaitSeconds"
#define VAR_ALLOWED_ONLY  "AllowedOnly"

#define CMD_STR_DISCONNECT  "Set(disconnect);"
#define CMD_STR_GETCOVERSZ  "Get(cover_size);"
#define CMD_STR_GETSCREENSZ "Get(screen_size);"

// TODO: Support L2CAP under FreeBSD
//#ifdef USE_BLUEZ
//#define USE_L2CAP 1
//#endif

enum RC_Code {
    RC_NOK = 0,
    RC_OK,
};

#endif
