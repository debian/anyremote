//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

#ifndef _MODE_H_
#define _MODE_H_

#include "str.h"

typedef struct Mt {
    string_t    *name;
    string_t    *parent;
    type_key*   keys;
    struct Mt   *next;
} mode;

void  printModes      (void);

char* getModeName     (void);  // return name of current mode

mode* getInternalMode (void);
mode* getDefaultMode  (void);
mode* getCurrentMode  (void);

mode* addMode         (const char *name, const char *parent);
mode* findMode        (const char *name);
void  switchMode      (const char *modeName);
void  setCurrentMode  (mode* m);

void  forgetModes     (void); // transfer ownership of modes
mode* getModes        (void);

void  freeModes       (mode* pmodes);  // if parameter is NULL, delete all modes stuff

#endif
