//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

#ifndef _PR_BTSPP_H_
#define _PR_BTSPP_H_

#include "peer.h"

int  btsppFD    (ConnectInfo* conn);
int  btsppOpen  (ConnectInfo* conn);
int  btsppListen(ConnectInfo* conn);
int  btsppAccept(ConnectInfo* conn);
int  btsppWrite (ConnectInfo* conn, dMessage* msg);
void btsppReset (ConnectInfo* conn);
void btsppClose (ConnectInfo* conn, int final);

#endif
