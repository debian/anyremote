//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

//
// Message queue related wrappers
//

#ifndef _PEER_H_
#define _PEER_H_ 1

#include "list.h"
#include "dispatcher.h"
#include "parse.h"

enum PeerState {
    PEER_DISCONNECTED = 0,
    PEER_WAIT_LISTEN,
    PEER_WAIT_ACCEPT,
    PEER_CONNECTED 
};

typedef struct {
    char* lastValues    [ID_SET_MAX];
    int   lastValuesSize[ID_SET_MAX];
} DCache;

typedef struct {
    int       id;
    int       mode;
    int       state;
    int       port;
    void*     connectionData;    // specific to connection
    DCache*   cache;   
    string_t* portStr;
} ConnectInfo;

int  definePeers    (void);
int  openPeers      (void);         // EXIT_OK/EXIT_NOK
int  setupPeersPre  (void);         // -1/1
int  disconnectPeers(void);         // -1/1
void closePeers     (int final);
void freePeers      (void);
int  processPeers   (void);         // EOF/0/N>0

int  readPeer  (int fd, char* buffer, int max);

int  writePeers     (dMessage* dm);
int  writeFilePeers (dMessage* dm);
int  writeBytesPeers(char* command);

int writeCKPD(dMessage* dm);
//int writeCMER(dMessage* dm);
void writeToFrontEnd(const char *buf);

//int  isAtMode      (void);          // EXIT_OK/EXIT_NOK
int  isAtModeDuplex(void);          // EXIT_OK/EXIT_NOK
int  isServerMode  (void);          // EXIT_OK/EXIT_NOK
//int  isServerModeNoWeb(void);       // EXIT_OK/EXIT_NOK
//int  isWebServer   (void);          // EXIT_OK/EXIT_NOK
int  connected     (void);          // EXIT_OK/EXIT_NOK
int  haveConnectionless(void);      // 0/1
int  needExit      (void);          // EXIT_OK/EXIT_NOK
int  needFinalizer (void);          // EXIT_OK/EXIT_NOK
int  needAtMainMenuReturn(int peer); // EXIT_OK/EXIT_NOK
void sendIViewerHeartbeat(void);

int  getIViewerTcpPort(void);	    // <port>/-1	

void      connectNotify(int peer);
boolean_t checkActiveCall();
boolean_t hasActiveCall  ();
void getClientSize(int peer, int fd);

int isDataOld(ConnectInfo* peer, int subtype, const char* data, int size);

#endif
