//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

//
// Message queue related wrappers
//

#ifndef _THREAD_H_
#define _THREAD_H_ 1

#ifdef USE_GLIB

#include "glib.h"
typedef GThreadFunc ThreadFunction;

#else

typedef void* (*ThreadFunction) (void* data);

#endif

enum ThreadID {
    T_DISP = 0,
    T_EXEC,
    T_AVAHI,
    T_MAX
};

enum ThreadParam {
    JOINABLE = 0,
    DETACHED,
};

void threadInit  (void);
int  threadNew   (int id, ThreadFunction func, void* data, int joinable);
void threadJoin  (int id);
void threadExit  (int id);  // terminates the current thread, call only from right place !!
int  threadExists(int id);

#endif
