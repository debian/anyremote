//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

#include <stdio.h>
#include "utils.h"

//
// Mutex related wrappers
//

#include "common.h"
#include "mutex.h"

#ifdef USE_GLIB

#include <glib.h>

#if GLIB_MAJOR_VERSION >=2 && GLIB_MINOR_VERSION >= 32

static GMutex    _mutexes[M_MAX];

int mutexNew(int id)
{
     if (id >=0 && id < M_MAX) {
 	 return RC_OK;
     }
     return RC_NOK;
}

void mutexLock(int id)
{
    if (id >=0 && id < M_MAX) {
        //printf("mutexLock %d\n", id);
        g_mutex_lock(&_mutexes[id]);
    }
}

void mutexUnlock(int id)
{
    if (id >=0 && id < M_MAX) {
        g_mutex_unlock(&_mutexes[id]);
    }
}
 
void mutexRemove(int id)
{
}

int mutexExists(int id)
{
    return (id >=0 && id < M_MAX ? RC_OK : RC_NOK);
}

#else

// GLIB 2.31 and older

GMutex* _mutexes[M_MAX] = {NULL};

int mutexNew(int id)
{
     if (id >=0 && id < M_MAX && _mutexes[id] == NULL) {
         _mutexes[id] = g_mutex_new();
	     return RC_OK;
     }
     return RC_NOK;
}

void mutexLock(int id)
{
    if (id >=0 && id < M_MAX && _mutexes[id] != NULL) {
        //printf("mutexLock %d\n", id);
        g_mutex_lock(_mutexes[id]);
    }
}

void mutexUnlock(int id)
{
    if (id >=0 && id < M_MAX && _mutexes[id] != NULL) {
        //printf("mutexUnlock %d\n", id);
        g_mutex_unlock(_mutexes[id]);
    }
}
 
void mutexRemove(int id)
{
    if (id >=0 && id < M_MAX && _mutexes[id] != NULL) {
        g_mutex_free(_mutexes[id]);
 	    _mutexes[id] = NULL;
    }
}

int mutexExists(int id)
{
    return (id >=0 && id < M_MAX && _mutexes[id] != NULL ? RC_OK : RC_NOK);
}

#endif

#else

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

pthread_mutex_t* _mutexes[M_MAX] = {NULL};

int mutexNew(int id)
{
    //printf("mutexNew %d\n", id);
    
    if (id >=0 && id < M_MAX && _mutexes[id] == NULL) {
	pthread_mutex_t* m = malloc(sizeof( pthread_mutex_t));
	pthread_mutex_init(m, NULL);
	_mutexes[id] = m;
	//printf("mutexNew %d ok\n", id);
	return RC_OK;
     }
     return RC_NOK;
}

void mutexLock(int id)
{
    if (id >=0 && id < M_MAX && _mutexes[id] != NULL) {
        pthread_mutex_lock(_mutexes[id]);
    }
}

void mutexUnlock(int id)
{
    if (id >=0 && id < M_MAX && _mutexes[id] != NULL) {
        pthread_mutex_unlock(_mutexes[id]);
    }
}
 
void mutexRemove(int id)
{
    if (id >=0 && id < M_MAX && _mutexes[id] != NULL) {
        pthread_mutex_destroy(_mutexes[id]);
        free(_mutexes[id]);
        _mutexes[id] = NULL;
    }
}

int mutexExists(int id)
{
    return (id >=0 && id < M_MAX && _mutexes[id] != NULL ? RC_OK : RC_NOK);
}

#endif


