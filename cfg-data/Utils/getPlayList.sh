#!/bin/sh

if [ "x$1" = "x" ]; then
	APP=org.kde.amarok
else
	APP=$1
fi; 

LENGTH=`dbus-send --type=method_call --print-reply --dest=$APP /TrackList org.freedesktop.MediaPlayer.GetLength|tail -1|grep int|tr -s ' '|cut -f 3 -d ' '`

I=0

while [ $I -lt $LENGTH ]
do
   T=`dbus-send --type=method_call --print-reply --dest=$APP /TrackList org.freedesktop.MediaPlayer.GetMetadata int32:$I |awk '/.*string "title".*/{getline;print}'|grep string|cut -f 2 -d '"'`
   ni=`expr $I + 1`  
   echo "$ni.$T,"
   
   I=`expr $I + 1`
done
