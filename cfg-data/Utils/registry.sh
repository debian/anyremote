#!/bin/sh

# Params
#  1. $(TmpDir)
#  2. get/set/rm/keys
#  3. key
#  4. (optional) value

REGISTRY=anyremote.registry

if [ "x$1" = "x" ] || [ "x$2" = "x" ] || [ "x$3" = "x" ]; then
    exit 1;
fi

if [ ! -d "$1" ]; then
    mkdir "$1";
fi;

if [ ! -f "$1/$REGISTRY" ]; then
    touch "$1/$REGISTRY";
fi;

if [ "x$2" = "xset" ] && [ "x$4" = "x" ]; then
    exit 1;
fi;

if [ "x$2" = "xset" ]; then
    grep -v "^$3=" $1/$REGISTRY > $1/$REGISTRY.tmp
    mv $1/$REGISTRY.tmp $1/$REGISTRY
    echo "$3=$4" >> $1/$REGISTRY
    exit 0;
fi;

if [ "x$2" = "xget" ]; then
    VALUE=`grep "^$3=" $1/$REGISTRY|cut -f 2 -d '=' 2> /dev/null`
    echo $VALUE
    exit 0;
fi;

if [ "x$2" = "xrm" ]; then
    grep -v "^$3=" $1/$REGISTRY > $1/$REGISTRY.tmp
    mv $1/$REGISTRY.tmp $1/$REGISTRY
    exit 0;
fi;

if [ "x$2" = "xkeys" ]; then
    cat $1/$REGISTRY|cut -f 2 -d '='
    exit 0;
fi;

exit 1;
