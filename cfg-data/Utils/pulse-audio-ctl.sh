#!/bin/sh

#
# Must be called from anyremote configuration file as $(CfgDir)/Utils/pulse-audio-ctl.sh up|down|mute|set|get <sink name> [<volume>]
#

if [ "x$1" = "xup" ]; then
  HEXVOL=`pacmd dump | grep "set-sink-volume $2" | cut -d " " -f 3`
  VOL=`printf "%d" $HEXVOL`
  # about 5%
  NEWVOL=`expr $VOL + 3270`
  if [ $(($NEWVOL)) -gt $((0x10000)) ]
  then
    NEWVOL=65536
  fi
  pactl set-sink-volume $2 $NEWVOL 
fi

if [ "x$1" = "xdown" ]; then
  HEXVOL=`pacmd dump | grep "set-sink-volume $2" | cut -d " " -f 3`
  VOL=`printf "%d" $HEXVOL`
  # about 5%
  NEWVOL=`expr $VOL - 3270`
  if [ $(($NEWVOL)) -lt $((0x00000)) ]
  then
    NEWVOL=0
  fi
  pactl set-sink-volume $2 $NEWVOL 
fi

if [ "x$1" = "xmute" ]; then
  A=`pacmd dump | grep "set-sink-mute $2" | cut -d " " -f 3`
  if [ $A = "no" ]
  then
    pactl set-sink-mute $2 yes
  else
    pactl set-sink-mute $2 no
  fi
fi 

if [ "x$1" = "xset" ]; then
  VOL=`echo $3|tr -d '%'`
  NEWVOL=`echo "$VOL*65536/100"|bc`
  if [ $(($NEWVOL)) -lt $((0x00000)) ]
  then
    NEWVOL=0
  fi
  if [ $(($NEWVOL)) -gt $((0x10000)) ]
  then
    NEWVOL=65536
  fi
  pactl set-sink-volume $2 $NEWVOL;
fi; 

if [ "x$1" = "xget" ]; then
  HEXVOL=`pacmd dump | grep "set-sink-volume $2" | cut -d " " -f 3`
  VOL65=`printf "%d" $HEXVOL`
  VOL=`echo "$VOL65*100/65536"|bc`
  echo $VOL
fi; 
